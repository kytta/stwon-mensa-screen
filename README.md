# stwon-mensa-screen

Client for the [STW-ON Mensa API], optimized for bigger screens.

## Run

In development:

```sh
flask --debug run
```

## Licence

Copyright © 2022 [Nikita Karamov]\
Licenced under the [Apache License 2.0].

Uses [STW-ON Mensa API] endpoints.

---

This project is hosted on Codeberg:
<https://codeberg.org/kytta/stwon-mensa-screen.git>

[Apache License 2.0]: https://spdx.org/licenses/Apache-2.0.html
[Nikita Karamov]: https://www.kytta.dev/
[STW-ON Mensa API]: https://api.stw-on.de/
