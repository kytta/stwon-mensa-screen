import datetime
from decimal import Decimal

import requests
from flask import Flask, abort, render_template, request

API_URL = "https://sls.api.stw-on.de/v1"

app = Flask(__name__)


def get_json(path: str):
    response = requests.get(f"{API_URL}{path}")
    if not response.ok:
        abort(response.status_code)

    response_json = response.json()
    if "error" in response_json:
        abort(response_json["error"]["code"] or 503)

    return response_json


@app.template_filter("money_format")
def money_format(value: str) -> str:
    sign, digits, exp = Decimal(value).quantize(Decimal("0.01")).as_tuple()
    digits = list(map(str, digits))

    # result = ["\u20ac", "\u00a0"]
    result = []
    for _ in range(2):
        result.append(str(digits.pop()) if digits else "0")
    result.append(",")
    if not digits:
        result.append("0")
    while digits:
        result.append(digits.pop())
    if sign:
        result.append("-")

    return "".join(reversed(result))


@app.template_filter("date_format")
def date_format(date: datetime.date) -> str:
    if type(date) is str:
        date = datetime.date.fromisoformat(date)
    return date.strftime("%A, %d. %B %Y")


@app.route("/")
def index():
    """Displays all locations sorted by city.
    """
    locations = get_json("/location")

    cities = {}

    for location in locations:
        city = location["address"]["city"]
        if city not in cities:
            cities[city] = []
        cities[city].append(location)

    return render_template("index.html", cities=cities)


@app.route("/<int:location_id>")
def menu_at_location(location_id: int):
    """Displays menu for one location.

    :param location_id: ID of the location
    """
    date = request.args.get("date", datetime.date.today().isoformat())

    menu = get_json(f"/locations/{location_id}/menu/{date}")
    location = get_json(f"/locations/{location_id}")
    return render_template("menu.html",
                           menu=menu,
                           date=date,
                           location=location)
